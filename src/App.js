import React from 'react';

import { GlobalStyle } from './globalStyles';
import { StoryEditor } from 'modules/storyEditor';

const App = () => (
  <>
    <GlobalStyle />
    <StoryEditor />
  </>
);

export default App;
