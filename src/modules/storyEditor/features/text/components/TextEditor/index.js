import React, { memo, useLayoutEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import { Overlay, TextInput } from './styles';
import { Wrapper, Button } from 'modules/storyEditor/components/Actions/styles';

const moveCursorToEnd = (el) => {
  el.focus();
  if (el.innerText && document.createRange) {
    const selection = document.getSelection();
    const range = document.createRange();
    range.selectNodeContents(el);
    range.collapse(false);
    selection.removeAllRanges();
    selection.addRange(range);
    //range.detach();
  }
};

const _TextEditor = ({ initialText, onSubmit }) => {
  const inputRef = useRef();

  useLayoutEffect(() => {
    if (inputRef.current) {
      const input = inputRef.current;
      moveCursorToEnd(input);
    }
  }, [initialText]);

  const handleSubmit = () => {
    onSubmit(inputRef.current.innerHTML);
  };

  return (
    <Overlay>
      <Wrapper>
        <Button onClick={handleSubmit}>OK</Button>
      </Wrapper>
      <TextInput
        ref={inputRef}
        contentEditable="true"
        tabIndex="-1"
        role="textbox"
        dangerouslySetInnerHTML={{ __html: initialText }}
      ></TextInput>
    </Overlay>
  );
};

_TextEditor.propTypes = {
  initialText: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  onSubmit: PropTypes.func.isRequired,
};

_TextEditor.defaultProps = {
  initialText: '',
};

export const TextEditor = memo(_TextEditor);
