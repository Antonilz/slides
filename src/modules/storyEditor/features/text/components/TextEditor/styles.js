import styled from 'styled-components/macro';

import { Text } from 'modules/storyEditor/components/elements/Text';

export const Overlay = styled.div`
  display: flex;
  position: absolute;
  top: 0;
  left: 0;
  flex-direction: column;
  align-items: center;
  overflow: hidden;
  height: 100%;
  width: 100%;
  background: rgba(0, 0, 0, 0.5);
`;

export const TextInput = styled(Text)`
  margin-top: 72px;
  -webkit-user-select: text;
  user-select: text;
`;
