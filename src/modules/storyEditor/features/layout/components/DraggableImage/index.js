import React, { useEffect, useRef, memo, useState } from 'react';

import { draggablePropTypes } from '../../utils/draggablePropTypes';

import { Background, DraggableArea } from './styles';

const _DraggableImage = ({
  id,
  x,
  y,
  content,
  transform,
  parentWidth,
  parentHeight,
}) => {
  const ref = useRef();
  const imageRef = useRef();
  const isPositionSet = useRef(false);
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    if (imageRef.current) {
      if (imageRef.current.complete) {
        setIsLoaded(true);
      } else {
        imageRef.current.onload = () => {
          setIsLoaded(true);
        };
      }
    }
  }, []);

  useEffect(() => {
    const element = ref.current;
    if (
      ref.current &&
      !isPositionSet.current &&
      parentHeight > 0 &&
      parentWidth > 0 &&
      isLoaded
    ) {
      if (x || y) {
        element.style.top = y;
        element.style.left = x;
      } else {
        element.style.left = `${
          50 - (element.clientWidth / parentWidth / 2) * 100
        }%`;
        element.style.top = `${
          50 - (element.clientHeight / parentHeight / 2) * 100
        }%`;
      }
      element.style.transform = transform;
      isPositionSet.current = true;
    }
  }, [x, y, parentHeight, parentWidth, transform, isLoaded]);

  return (
    <DraggableArea ref={ref} id={id}>
      <Background ref={imageRef} src={content} />
    </DraggableArea>
  );
};

_DraggableImage.propTypes = draggablePropTypes;

_DraggableImage.defaultProps = {
  x: undefined,
  y: undefined,
  transform: '',
};

export const DraggableImage = memo(_DraggableImage);
