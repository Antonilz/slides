import styled from 'styled-components/macro';

export const Canvas = styled.div`
  display: flex;
  position: absolute;
  top: 0;
  left: 0;
  overflow: hidden;
  height: 100%;
  width: 100%;
  touch-action: none;
`;
