import { useRef } from 'react';

import { checkIsLineIntersecting } from '../../utils/checkIsLineIntersecting';
import {
  getPinchProps,
  getTransformProps,
  findClosestNumber,
} from './touchUtils';
import { vibrate } from 'utils/vibrate';

const TAP_MOVE_THRESHOLD = 5;

export const useTouch = ({ containerRef, elementsRef, onElementTap }) => {
  const lastPinchRef = useRef(null);
  const initialPinchRef = useRef(null);
  const grabbedElementRef = useRef(null);
  const lastGrabbedElementRef = useRef(null);

  const handleZoom = (ev) => {
    const touches = ev.touches;
    const initialPinch = initialPinchRef.current;
    const currentPinch =
      touches[0].identifier === initialPinch.touches[0].identifier &&
      touches[1].identifier === initialPinch.touches[1].identifier
        ? getPinchProps(touches) // the touches are in the correct order
        : touches[1].identifier === initialPinch.touches[0].identifier &&
          touches[0].identifier === initialPinch.touches[1].identifier
        ? getPinchProps(touches.reverse()) // the touches have somehow changed order
        : getPinchProps(touches);

    currentPinch.displacement = {
      x: currentPinch.center.x - initialPinch.center.x,
      y: currentPinch.center.y - initialPinch.center.y,
    };
    currentPinch.rotation = currentPinch.angle - initialPinch.angle;
    currentPinch.zoom = currentPinch.distance / initialPinch.distance;
    lastPinchRef.current = currentPinch;

    if (grabbedElementRef.current && currentPinch.zoom > 0.3) {
      const grabbedElement = grabbedElementRef.current;
      const {
        scale: initialScale,
        rotate: initialRotation,
      } = grabbedElement.initialTransform;
      const rotation = initialRotation + currentPinch.rotation;
      const closestGuide = findClosestNumber(rotation, 45);
      let rotationStyle = ` rotate(${rotation}deg)`;

      grabbedElement.allowSnapping =
        grabbedElement.allowSnapping || Math.abs(currentPinch.rotation) > 5;

      if (!grabbedElement.allowRotation) {
        rotationStyle = ` rotate(${grabbedElement.closestGuide}deg)`;
      } else if (
        Math.abs(closestGuide - rotation) < 5 &&
        grabbedElementRef.current.allowSnapping
      ) {
        vibrate(100);
        rotationStyle = ` rotate(${closestGuide}deg)`;
        grabbedElement.allowRotation = false;
        grabbedElement.closestGuide = closestGuide;
      }

      grabbedElement.node.style.transform = `scale(${Math.max(
        Math.min(currentPinch.zoom * initialScale, 10),
        0.3
      )})${rotationStyle}`;
      grabbedElement.node.style.left = `${
        parseFloat(grabbedElement.initialX) +
        (currentPinch.displacement.x / containerRef.current.clientWidth) * 100
      }%`;
      grabbedElement.node.style.top = `${
        parseFloat(grabbedElement.initialY) +
        (currentPinch.displacement.y / containerRef.current.clientHeight) * 100
      }%`;
    }
  };

  const handleTouchStart = (ev) => {
    ev.preventDefault();
    const firstTouch = ev.touches[0];
    const firstPoint = [firstTouch.clientX, firstTouch.clientY];
    const touches = ev.touches;
    let grabbedElements = [];

    if (ev.touches.length === 2) {
      const secondTouch = ev.touches[1];
      const secondPoint = [secondTouch.clientX, secondTouch.clientY];

      grabbedElements = elementsRef.current.filter((element) => {
        const { top, left, right, bottom } = element.getBoundingClientRect();
        const box = [left, top, right, bottom];
        const isIntersecting = checkIsLineIntersecting(
          firstPoint,
          secondPoint,
          box
        );
        return isIntersecting;
      });
    } else {
      const touchedElement = elementsRef.current.find(
        (element) => element === ev.target || element.contains(ev.target)
      );

      if (touchedElement) {
        grabbedElements = [touchedElement];
      }
    }

    initialPinchRef.current = {
      ...getPinchProps(touches),
      displacement: { x: 0, y: 0 },
      displacementVelocity: { x: 0, y: 0 },
      rotation: 0,
      rotationVelocity: 0,
      zoom: 1,
      zoomVelocity: 0,
      time: Date.now(),
    };
    lastPinchRef.current = initialPinchRef.current;

    if (grabbedElements.length > 0) {
      const prevGrabbedElement =
        grabbedElementRef.current && grabbedElementRef.current.node;
      const grabbedElement =
        grabbedElements.find((element) => element === prevGrabbedElement) ||
        (lastGrabbedElementRef.current &&
          grabbedElements.find(
            (element) => element === lastGrabbedElementRef.current.node
          )) ||
        grabbedElements[0];
      grabbedElementRef.current = {
        node: grabbedElement,
        initialTransform: getTransformProps(grabbedElement.style.transform),
        initialX: grabbedElement.style.left,
        initialY: grabbedElement.style.top,
        allowRotation: true,
        allowSnapping: false,
        closestGuide: 0,
      };
    }
  };

  const handleTouchMove = (ev) => {
    const touches = ev.touches;
    ev.preventDefault();
    if (touches.length === 1) {
      const currentPinch = getPinchProps(touches);
      const initialPinch = initialPinchRef.current;
      const grabbedElement = grabbedElementRef.current;

      if (initialPinch) {
        currentPinch.displacement = {
          x: currentPinch.center.x - initialPinch.center.x,
          y: currentPinch.center.y - initialPinch.center.y,
        };

        if (grabbedElement) {
          grabbedElement.node.style.left = `${
            ((currentPinch.center.x - initialPinch.center.x) /
              containerRef.current.clientWidth) *
              100 +
            parseFloat(grabbedElement.initialX)
          }%`;
          grabbedElement.node.style.top = `${
            ((currentPinch.center.y - initialPinch.center.y) /
              containerRef.current.clientHeight) *
              100 +
            parseFloat(grabbedElement.initialY)
          }%`;
        }
        lastPinchRef.current = currentPinch;
      }
    }

    if (touches.length === 2 && initialPinchRef.current) {
      handleZoom(ev);
    }
  };

  const handleTouchEnd = (ev) => {
    ev.preventDefault();
    const lastPinch = lastPinchRef.current;
    const grabbedElement = grabbedElementRef.current;

    if (lastPinch && lastPinch.touches.length === 1 && grabbedElement) {
      const { x, y } = lastPinch.displacement;
      if (
        Math.abs(x) <= TAP_MOVE_THRESHOLD &&
        Math.abs(y) <= TAP_MOVE_THRESHOLD
      ) {
        onElementTap && onElementTap(grabbedElement.node.id);
      }
    }
    if (grabbedElement) {
      // TODO event triggers twice for 2 touches, need to pick correct element
      lastGrabbedElementRef.current = grabbedElement;
    }

    initialPinchRef.current = lastPinchRef.current = grabbedElementRef.current = null;
  };

  return { handleTouchStart, handleTouchMove, handleTouchEnd };
};
