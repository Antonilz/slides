import PropTypes from 'prop-types';

export const draggablePropTypes = {
  id: PropTypes.string.isRequired,
  x: PropTypes.number,
  y: PropTypes.number,
  parentWidth: PropTypes.number.isRequired,
  parentHeight: PropTypes.number.isRequired,
  transform: PropTypes.string,
  content: PropTypes.node.isRequired,
};
