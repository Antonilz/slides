import React from 'react';
import PropTypes from 'prop-types';

import { Wrapper, Button } from './styles';

const actions = [
  { icon: 'I', name: 'image' },
  { icon: 'T', name: 'text' },
];

export const Actions = ({ onActionClick }) => (
  <Wrapper>
    {actions.map(({ icon, name }) => (
      <Button onClick={() => onActionClick(name)}>{icon}</Button>
    ))}
  </Wrapper>
);

Actions.propTypes = {
  onActionClick: PropTypes.func.isRequired,
};
