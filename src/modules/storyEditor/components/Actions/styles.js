import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  display: flex;
  position: absolute;
  z-index: 10;
  right: 20px;
  top: 10px;

  > * + * {
    margin-left: 15px;
  }
`;

export const Button = styled.button`
  font-size: 32px;
  font-weight: bold;
  background-color: transparent;
  color: #fff;
  border: 0;
  outline: 0;
  height: 40px;
  min-width: 40px;
  padding: 4px;
`;
