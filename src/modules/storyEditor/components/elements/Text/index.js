import styled from 'styled-components/macro';

export const Text = styled.span`
  display: block;
  max-width: calc(100% - 128px);
  text-align: center;
  word-wrap: break-word;
  color: white;
  text-shadow: rgba(150, 150, 150, 0.3) 0px 1px 2px;
  font-size: 32px;
  font-weight: 500;
  line-height: 32px;
  outline: 0;
  padding: 10px;
`;
