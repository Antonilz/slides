import React, { useEffect } from 'react';

import { Wrapper, Story } from './styles';

export const Layout = ({ children }) => {
  // disable iOS body zoom behavior
  useEffect(() => {
    document.addEventListener(
      'touchmove',
      (ev) => {
        ev.preventDefault();
      },
      { passive: false }
    );
    document.addEventListener(
      'touchstart',
      (event) => {
        if (event.touches.length > 1) {
          event.preventDefault();
          event.stopPropagation();
        }
      },
      { passive: false }
    );
  }, []);

  return (
    <Wrapper>
      <Story>{children}</Story>
    </Wrapper>
  );
};
