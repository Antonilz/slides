import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #222;
  height: 100%;
`;

export const Story = styled.div`
  height: 100%;
  width: 100vw;
  background-color: teal;
`;
