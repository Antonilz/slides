import React, { useState, useRef, useCallback } from 'react';

import { createUUID } from 'utils/createUUID';
import { Actions } from '../Actions';
import { Layout } from '../Layout';
import { TextEditor } from '../../features/text/components/TextEditor';
import { LayoutEditor } from '../../features/layout/components/LayoutEditor';
import { ImageUploader } from '../ImageUploader';

export const StoryEditor = () => {
  const [currentEditor, setCurrentEditor] = useState('main');
  const [elements, setElements] = useState({});
  const [currentElementId, setCurrentElementId] = useState(null);
  const elementsRef = useRef([]);
  const imageUploaderRef = useRef();

  const addElement = useCallback((info) => {
    setElements((prevElements) => ({
      ...prevElements,
      [createUUID()]: info,
    }));
  }, []);

  const addImage = async (image) => {
    const blobUrl = URL.createObjectURL(image);

    addElement({
      type: 'background',
      content: blobUrl,
      transform: '',
    });
  };

  const onActionClick = async (editorName) => {
    if (editorName === 'image') {
      return imageUploaderRef.current.open();
    }
    setCurrentEditor(editorName);
  };

  const onTextSubmit = (text) => {
    setElements(
      elementsRef.current.reduce((acc, elementNode) => {
        const elementInfo = elements[elementNode.id];
        return {
          ...acc,
          [elementNode.id]: {
            type: elementInfo.type,
            content: elementInfo.content,
            x: elementNode.style.left,
            y: elementNode.style.top,
            transform: elementNode.style.transform,
          },
        };
      }, {})
    );
    if (currentElementId) {
      setElements((prevElements) => ({
        ...prevElements,
        [currentElementId]: {
          ...prevElements[currentElementId],
          content: text,
        },
      }));
      setCurrentElementId(null);
    } else {
      addElement({
        type: 'text',
        content: text,
        transform: '',
      });
    }
    setCurrentEditor('main');
  };

  const onElementTap = (id) => {
    setCurrentElementId(id);
    if (elements[id] && elements[id].type === 'text') {
      setCurrentEditor('text');
    }
  };

  return (
    <Layout>
      {currentEditor === 'main' && (
        <>
          <Actions onActionClick={onActionClick} />
          <LayoutEditor
            elements={elements}
            ref={elementsRef}
            onElementTap={onElementTap}
          />
        </>
      )}
      {currentEditor === 'text' && (
        <TextEditor
          onSubmit={onTextSubmit}
          {...(elements[currentElementId] && {
            initialText: elements[currentElementId].content,
          })}
        />
      )}
      <ImageUploader ref={imageUploaderRef} onImageSelect={addImage} />
    </Layout>
  );
};
