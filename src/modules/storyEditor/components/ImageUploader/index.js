import React, { useRef, useImperativeHandle, forwardRef } from 'react';

export const ImageUploader = forwardRef(({ onImageSelect }, ref) => {
  const inputRef = useRef();

  const onInputChange = () => {
    const files = Array.from(inputRef.current.files);
    onImageSelect && onImageSelect(files[0]);
  };

  useImperativeHandle(
    ref,
    () => ({
      open: () => inputRef.current.click(),
    }),
    []
  );

  return (
    <>
      <input
        style={{ display: 'none' }}
        onChange={onInputChange}
        accept="image/jpeg"
        type="file"
        ref={inputRef}
      />
    </>
  );
});
