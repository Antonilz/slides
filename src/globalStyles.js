import { createGlobalStyle } from 'styled-components/macro';

export const GlobalStyle = createGlobalStyle`
  #root {
    height: 100%;
  }

  html {
    height: 100%;
    overflow: auto;
    margin: 0;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
  -webkit-font-smoothing: subpixel-antialiased;
  -moz-osx-font-smoothing: grayscale;
  }
  

  body {
    position: relative;
    margin: 0;
    min-width: 320px;
    height: 100%;
    overflow: auto;
    width: 100%;
    font-size: 14px;
    touch-action: none;
  }

  body,
  html {
    width: 100%;
    height: 100%;
    margin: 0;
    overflow: auto;
  }
`;
